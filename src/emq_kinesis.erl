-module(emq_kinesis).

-include_lib("emqttd/include/emqttd.hrl").

-include_lib("emqttd/include/emqttd_protocol.hrl").

-include_lib("emqttd/include/emqttd_internal.hrl").

-define(APP, emq_kinesis).

-export([load/0, unload/0]).

-export([on_message_publish/2]).

load() ->
  emqttd:hook('message.publish', fun ?MODULE:on_message_publish/2, [application:get_env(?APP)]).

on_message_publish(Message = #mqtt_message{topic = <<"$SYS/", _/binary>>}, _Env) ->
  {ok, Message};

on_message_publish(Message, _Env) ->
  Topic = Message#mqtt_message.topic,
  Payload = Message#mqtt_message.payload,
%%  Fun = fun(Worker) ->
%%    ok = emq_kinesis_worker:add_queue(Worker, Topic, Payload)
%%        end,
%%  ok = octopus:perform(kinesis, Fun),
  ok = hottub:execute(kinesis,
    fun(Worker) ->
      ok = emq_kinesis_worker:add_queue(Worker, Topic, Payload)
    end),
  {ok, Message}.

unload() ->
  emqttd:unhook('message.publish', fun ?MODULE:on_message_publish/2).


