-module(emq_kinesis_app).

-behaviour(application).

-export([start/2, stop/1]).

-define(APP, emq_kinesis).

start(_StartType, _StartArgs) ->
  {ok, Sup} = emq_kinesis_sup:start_link(),
  ok = start_pool(),
  emq_kinesis:load(),
  emq_kinesis_config:register(),
  {ok, Sup}.

stop(_State) ->
  emq_kinesis:unload(),
  emq_kinesis_config:unregister().

start_pool() ->
  PoolSize = application:get_env(?APP, pool_size),
%%  PoolOpts = [
%%    {pool_size, PoolSize},
%%    {worker, emq_kinesis_worker}
%%  ],
%%  ok = octopus:start_pool(kinesis, PoolOpts, []).
  {ok, Pid} = hottub:start_link(kinesis, PoolSize, emq_kinesis_worker, start_link, []),
  ok.
