-module(emq_kinesis_sup).

-behaviour(supervisor).

-export([start_link/0]).

-export([init/1]).

-define(M, emq_kinesis).

start_link() ->
  supervisor:start_link({local, ?MODULE}, ?MODULE, []).

init([]) ->
  {ok, { {one_for_one, 5, 10}, []} }.
