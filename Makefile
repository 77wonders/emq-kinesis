PROJECT = emq_kinesis
PROJECT_DESCRIPTION = EMQ Kinesis
PROJECT_VERSION = 2.3.0

DEPS = lager hottub erlcloud clique

dep_lager = git https://github.com/basho/lager master
dep_hottub = git https://github.com/bfrog/hottub master
dep_erlcloud  = git https://github.com/erlcloud/erlcloud master
dep_clique  = git https://github.com/emqtt/clique

BUILD_DEPS = emqttd cuttlefish
dep_emqttd     = git https://github.com/emqtt/emqttd master
dep_cuttlefish = git https://github.com/emqtt/cuttlefish

NO_AUTOPATCH = cuttlefish

ERLC_OPTS += +debug_info
ERLC_OPTS += +'{parse_transform, lager_transform}'

COVER = true

include erlang.mk

app:: rebar.config

app.config::
	./deps/cuttlefish/cuttlefish -l info -e etc/ -c etc/emq_kinesis.conf -i priv/emq_kinesis.schema -d data